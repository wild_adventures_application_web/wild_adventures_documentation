# Compte rendu de réunion

## 13/12/2018

  * Discussion autour des technologies à utiliser
  * Discussion à propos de l'architecture micro-service
  * Présentation des user stories réalisées par Marjane et validation par chef de projet
  * Répartition des tâches pour le sprint 0

## 04/01/2019
  * Discussion sur la répartition des tâches
    * Confirmation que les tâches sont bien réparties
  * Rappel de l'importance de la mise à jour du planning des Sprint
  * L'emsemble des tâches ont été créés sur trello
  * Bien s'affecter les tâches sur trello (idéalement à l'avance)
  * Côté developpement, pour le sprint suivant:
    * Arnaud: micoservice adventure
    * Marjane: microservice payment

## 11/01/2019
  * Discussion sur la charge de travail à fournir pour sécuriser l'application.
  * Discussion sur la répartition des tâches pour le sprint suivant
    * Marjane: finalisation des services users et location puis Spring-security
    * Arnaud: finalisation du service Comments et création des micro-services payment et order. Intégration de adventure et comment avec les microservice-edges développés par Marjane
  * Mise à jour du planning des Sprints à faire: Arnaud
  * Démonstration
    * Marjane: services users et location fonctionels et intégrés avec le micro-service edge ZUUL
    * Arnaud: microservice adventure fonctionnel et intégration continue avec tests implémenté

## 18/01/2019
  * Bilan du sprint 11/01 -> 18/01
    * Arnaud: les microservices adventure et commentsonst terminés mais pas connectés avec les microservices edges. Des difficultés ont été rencontrés pour l'intégration de feign (requeter les comments).
    * Marjane: Les services users et location sont terminés et de nombreux tests unitaires ont été ajoutés.
  * Echange à propos de la mise en place de la sécurité et du service de payment
  * Définition du prochain sprint
    * Arnaud: Lier les micoservices adventure et comment aux microservices edges + commencer le microservice order.
    * Marjane: Mise en place de la sécurité + mettre en place l'intégration continue sur les services users et location.

## 25/01/2019
  * Bilan du sprint 18/01 -> 25/01
    * Arnaud: Adaptation de la configuration des microservices adventures et comments pour pouvoir s'associer au micro-service edge. Intégration avec les microservices ok. Mise en place de l'intégration continue sur les micro-services edge et avancement sur une solution de déploiment (docker-compose).
    * Marjane: Monté en compétence sur spring-security. l'implémentation suit son cours.
  * Définition du prochain sprint
    * Arnaud: Avancer le microservice order.
    * Marjane: Finir la mise en place de la sécurité + monté en compétence an Angular.

## 01/02/2019
  * Pas de réunion de fin de sprint. Le chef de projet est en vacance.

## 08/02/2019
  * Bilan du sprint 25/01 -> 08/02
    * Arnaud: Microservice order bien avancé mais des difficultés rencontrés lors de la mise en place des tests d'intégration.
    * Marjane: Sécurité mise en place en grande partie. Monté en compétence sur Angular.
  * Définition du prochain sprint
    * Arnaud: Régler le problème des tests du microservice order et commencer le microservice payment.
    * Marjane: Finir la mise en place de la sécurité.

## 15/02/2019
  * Bilan du sprint 08/02 -> 15/02
    * Arnaud: Microservice order terminé et microservice payment entamé.
    * Marjane: Mise en place de la sécurité terminée.
  * Définition du prochain sprint
    * Arnaud: Avancer sur le microservice payment.
    * Marjane: Commencer l'implémentation du client.

## 22/02/2019
  * Bilan du sprint 15/02 -> 22/02
    * Développements terminés du back-end (Arnaud et Marjane)
    * Début d'implémentation du client (Arnaud et Marjane)
  * Définition du prochain sprint
    * Intégration des dockerfiles sur tous les MS
    * Continuer développement du client

## 01/03/2019
  * Bilan du sprint 22/02 -> 1/03
    * Intégration des dockerfiles sur tous les MS (Arnaud)
    * Difficultés rencontrés par Marjane pour lancer les images docker > utilisation de services mock pour continuer à avancer le client
  * Définition du prochain sprint
    * Arnaud absent
    * Intégrer le client avec Zuul
    * Continuer développement du client

## 08/03/2019
  * Bilan du sprint 01/03 -> 8/03
    * Retravail de la sécurité, changement pour des jwt tokens (Marjane)
    * Avancé du client
  * Définition du prochain sprint
    * Arnaud absent
    * Continuer développement du client (Marjane)

## 15/03/2019
  * Bilan du sprint 08/03 -> 15/03
    * Difficultés de coordinations entre Marjane et Arnaud
    * Avancé du client
  * Définition du prochain sprint
    * Se coordiner pour avoir une solution de déploiment (docker) intégrant tout les composants et notamment le nouveau microservice Auth dévelopé par Marjane
    * Continuer développement du client (Marjane, Arnaud)

## 22/03/2019
  * Bilan du sprint 15/03 -> 22/03
    * Back version stable
    * Back déployé dans le cloud (Arnaud)
    * Avancé du client (Arnaud, Marjane)
  * Définition du prochain sprint
    * Terminer les fonctionnalités de commande et de payment dans le front (Arnaud)
    * Ajouter les microservices de maintenance (Admin, Zipkin) (Marjane)
